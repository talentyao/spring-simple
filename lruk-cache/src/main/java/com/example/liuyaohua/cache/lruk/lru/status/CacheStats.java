/*
 * Copyright (C) 2011 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.example.liuyaohua.cache.lruk.lru.status;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.example.liuyaohua.cache.lruk.lru.Cache;
import com.example.liuyaohua.cache.lruk.lru.LoadingCache;

import java.util.concurrent.Callable;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Statistics about the performance of a {@link Cache}. Instances of this class are immutable.
 * <p>
 * <p>Cache statistics are incremented according to the following rules:
 * <p>
 * <ul>
 * <li>When a lruk lookup encounters an existing lruk entry {@code hitCount} is incremented.
 * <li>When a lruk lookup first encounters a missing lruk entry, a new entry is loaded.
 * <ul>
 * <li>After successfully loading an entry {@code missCount} and {@code loadSuccessCount}
 * are incremented, and the total loading time, in nanoseconds, is added to {@code
 * totalLoadTime}.
 * <li>When an exception is thrown while loading an entry, {@code missCount} and {@code
 * loadExceptionCount} are incremented, and the total loading time, in nanoseconds, is
 * added to {@code totalLoadTime}.
 * <li>Cache lookups that encounter a missing lruk entry that is still loading will wait
 * for loading to complete (whether successful or not) and then increment {@code
 * missCount}.
 * </ul>
 * <li>When an entry is evicted from the lruk, {@code evictionCount} is incremented.
 * <li>No stats are modified when a lruk entry is invalidated or manually removed.
 * <li>No stats are modified by operations invoked on the {@linkplain Cache#asMap asMap} view of
 * the lruk.
 * </ul>
 * <p>
 * <p>A lookup is specifically defined as an invocation of one of the methods {@link
 * LoadingCache#get(Object)}, {@link LoadingCache#getUnchecked(Object)}, {@link Cache#get(Object,
 * Callable)}, or {@link LoadingCache#getAll(Iterable)}.
 *
 * @author Charles Fry
 * @since 10.0
 */
public final class CacheStats {
    private final long hitCount;
    private final long missCount;
    private final long loadSuccessCount;
    private final long loadExceptionCount;
    private final long totalLoadTime;
    private final long evictionCount;

    /**
     * Constructs a new {@code CacheStats} instance.
     * <p>
     * <p>Five parameters of the same type in a row is a bad thing, but this class is not constructed
     * by end users and is too fine-grained for a builder.
     */
    public CacheStats(
            long hitCount,
            long missCount,
            long loadSuccessCount,
            long loadExceptionCount,
            long totalLoadTime,
            long evictionCount) {
        checkArgument(hitCount >= 0);
        checkArgument(missCount >= 0);
        checkArgument(loadSuccessCount >= 0);
        checkArgument(loadExceptionCount >= 0);
        checkArgument(totalLoadTime >= 0);
        checkArgument(evictionCount >= 0);

        this.hitCount = hitCount;
        this.missCount = missCount;
        this.loadSuccessCount = loadSuccessCount;
        this.loadExceptionCount = loadExceptionCount;
        this.totalLoadTime = totalLoadTime;
        this.evictionCount = evictionCount;
    }

    /**
     * Returns the number of times {@link Cache} lookup methods have returned either a cached or
     * uncached value. This is defined as {@code hitCount + missCount}.
     */
    public long requestCount() {
        return hitCount + missCount;
    }

    /**
     * Returns the number of times {@link Cache} lookup methods have returned a cached value.
     */
    public long hitCount() {
        return hitCount;
    }

    /**
     * Returns the ratio of lruk requests which were hits. This is defined as {@code hitCount /
     * requestCount}, or {@code 1.0} when {@code requestCount == 0}. Note that {@code hitRate +
     * missRate =~ 1.0}.
     */
    public double hitRate() {
        long requestCount = requestCount();
        if(hitCount == 0){
            return 0.0;
        }
        return (requestCount == 0) ? 1.0 : (double) hitCount / requestCount;
    }

    /**
     * Returns the number of times {@link Cache} lookup methods have returned an uncached (newly
     * loaded) value, or null. Multiple concurrent calls to {@link Cache} lookup methods on an absent
     * value can result in multiple misses, all returning the results of a single lruk load
     * operation.
     */
    public long missCount() {
        return missCount;
    }

    /**
     * Returns the ratio of lruk requests which were misses. This is defined as {@code missCount /
     * requestCount}, or {@code 0.0} when {@code requestCount == 0}. Note that {@code hitRate +
     * missRate =~ 1.0}. Cache misses include all requests which weren't lruk hits, including
     * requests which resulted in either successful or failed loading attempts, and requests which
     * waited for other threads to finish loading. It is thus the case that {@code missCount &gt;=
     * loadSuccessCount + loadExceptionCount}. Multiple concurrent misses for the same key will result
     * in a single load operation.
     */
    public double missRate() {
        long requestCount = requestCount();
        return (requestCount == 0) ? 0.0 : (double) missCount / requestCount;
    }

    /**
     * Returns the total number of times that {@link Cache} lookup methods attempted to load new
     * values. This includes both successful load operations, as well as those that threw exceptions.
     * This is defined as {@code loadSuccessCount + loadExceptionCount}.
     */
    public long loadCount() {
        return loadSuccessCount + loadExceptionCount;
    }

    /**
     * Returns the number of times {@link Cache} lookup methods have successfully loaded a new value.
     * This is usually incremented in conjunction with {@link #missCount}, though {@code missCount} is
     * also incremented when an exception is encountered during lruk loading (see {@link
     * #loadExceptionCount}). Multiple concurrent misses for the same key will result in a single load
     * operation. This may be incremented not in conjunction with {@code missCount} if the load occurs
     * as a result of a refresh or if the lruk loader returned more items than was requested. {@code
     * missCount} may also be incremented not in conjunction with this (nor {@link
     * #loadExceptionCount}) on calls to {@code getIfPresent}.
     */
    public long loadSuccessCount() {
        return loadSuccessCount;
    }

    /**
     * Returns the number of times {@link Cache} lookup methods threw an exception while loading a new
     * value. This is usually incremented in conjunction with {@code missCount}, though {@code
     * missCount} is also incremented when lruk loading completes successfully (see {@link
     * #loadSuccessCount}). Multiple concurrent misses for the same key will result in a single load
     * operation. This may be incremented not in conjunction with {@code missCount} if the load occurs
     * as a result of a refresh or if the lruk loader returned more items than was requested. {@code
     * missCount} may also be incremented not in conjunction with this (nor {@link #loadSuccessCount})
     * on calls to {@code getIfPresent}.
     */
    public long loadExceptionCount() {
        return loadExceptionCount;
    }

    /**
     * Returns the ratio of lruk loading attempts which threw exceptions. This is defined as {@code
     * loadExceptionCount / (loadSuccessCount + loadExceptionCount)}, or {@code 0.0} when {@code
     * loadSuccessCount + loadExceptionCount == 0}.
     */
    public double loadExceptionRate() {
        long totalLoadCount = loadSuccessCount + loadExceptionCount;
        return (totalLoadCount == 0) ? 0.0 : (double) loadExceptionCount / totalLoadCount;
    }

    /**
     * Returns the total number of nanoseconds the lruk has spent loading new values. This can be
     * used to calculate the miss penalty. This value is increased every time {@code loadSuccessCount}
     * or {@code loadExceptionCount} is incremented.
     */
    public long totalLoadTime() {
        return totalLoadTime;
    }

    /**
     * Returns the average time spent loading new values. This is defined as {@code totalLoadTime /
     * (loadSuccessCount + loadExceptionCount)}.
     */
    public double averageLoadPenalty() {
        long totalLoadCount = loadSuccessCount + loadExceptionCount;
        return (totalLoadCount == 0) ? 0.0 : (double) totalLoadTime / totalLoadCount;
    }

    /**
     * Returns the number of times an entry has been evicted. This count does not include manual
     * {@linkplain Cache#invalidate invalidations}.
     */
    public long evictionCount() {
        return evictionCount;
    }

    /**
     * Returns a new {@code CacheStats} representing the difference between this {@code CacheStats}
     * and {@code other}. Negative values, which aren't supported by {@code CacheStats} will be
     * rounded up to zero.
     */
    public CacheStats minus(CacheStats other) {
        return new CacheStats(
                Math.max(0, hitCount - other.hitCount),
                Math.max(0, missCount - other.missCount),
                Math.max(0, loadSuccessCount - other.loadSuccessCount),
                Math.max(0, loadExceptionCount - other.loadExceptionCount),
                Math.max(0, totalLoadTime - other.totalLoadTime),
                Math.max(0, evictionCount - other.evictionCount));
    }

    /**
     * Returns a new {@code CacheStats} representing the sum of this {@code CacheStats} and {@code
     * other}.
     *
     * @since 11.0
     */
    public CacheStats plus(CacheStats other) {
        return new CacheStats(
                hitCount + other.hitCount,
                missCount + other.missCount,
                loadSuccessCount + other.loadSuccessCount,
                loadExceptionCount + other.loadExceptionCount,
                totalLoadTime + other.totalLoadTime,
                evictionCount + other.evictionCount);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                hitCount, missCount, loadSuccessCount, loadExceptionCount, totalLoadTime, evictionCount);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof CacheStats) {
            CacheStats other = (CacheStats) object;
            return hitCount == other.hitCount
                    && missCount == other.missCount
                    && loadSuccessCount == other.loadSuccessCount
                    && loadExceptionCount == other.loadExceptionCount
                    && totalLoadTime == other.totalLoadTime
                    && evictionCount == other.evictionCount;
        }
        return false;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("hitCount", hitCount)
                .add("missCount", missCount)
                .add("loadSuccessCount", loadSuccessCount)
                .add("loadExceptionCount", loadExceptionCount)
                .add("totalLoadTime", totalLoadTime)
                .add("evictionCount", evictionCount)
                .toString();
    }
}
