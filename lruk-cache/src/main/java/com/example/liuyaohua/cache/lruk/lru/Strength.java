package com.example.liuyaohua.cache.lruk.lru;

import com.example.liuyaohua.cache.lruk.lru.entry.ReferenceEntry;
import com.google.common.base.Equivalence;
import com.example.liuyaohua.cache.lruk.lru.entry.ReferenceEntry;
import com.example.liuyaohua.cache.lruk.lru.reference.StrongValueReference;
import com.example.liuyaohua.cache.lruk.lru.reference.ValueReference;
import com.example.liuyaohua.cache.lruk.lru.reference.WeightedStrongValueReference;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-26 00:13
 * 说明 ...
 */
public enum Strength {
    /*
     * TODO(kevinb): If we strongly reference the value and aren't loading, we needn't wrap the
     * value. This could save ~8 bytes per entry.
     */

    STRONG {
        @Override
        public <K, V> ValueReference<K, V> referenceValue(Segment<K, V> segment, ReferenceEntry<K, V> entry, V value, int weight) {
            return (weight == 1)
                    ? new StrongValueReference<K, V>(value)
                    : new WeightedStrongValueReference<K, V>(value, weight);
        }

        @Override
        public Equivalence<Object> defaultEquivalence() {
            return Equivalence.equals();
        }
    };

    /**
     * Creates a reference for the given value according to this value strength.
     */
    public abstract <K, V> ValueReference<K, V> referenceValue(
            Segment<K, V> segment, ReferenceEntry<K, V> entry, V value, int weight);

    /**
     * Returns the default equivalence strategy used to compare and hash keys or values referenced
     * at this strength. This strategy will be used unless the usercontext explicitly specifies an
     * alternate strategy.
     */
    public abstract Equivalence<Object> defaultEquivalence();
}
