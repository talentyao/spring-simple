package com.example.liuyaohua.cache.lruk.sync;

import java.io.Serializable;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2017-11-08 15:31
 * <p>说明 同步类型：房东、门店、房屋
 */
public enum EnumLrukSyncType implements Serializable {
    /**
     *
     */
    UNKNOW(-1, "未知"),
    LANDLORD(0, "房东"),
    HOTEL(1, "门店"),
    HOUSE(2, "房屋");

    private int code;
    private String desc;

    EnumLrukSyncType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static EnumLrukSyncType codeOf(int code) {
        for (EnumLrukSyncType item : EnumLrukSyncType.values()) {
            if (item == null) {
                continue;
            }
            if (code == item.code) {
                return item;
            }
        }
        return null;
    }
}