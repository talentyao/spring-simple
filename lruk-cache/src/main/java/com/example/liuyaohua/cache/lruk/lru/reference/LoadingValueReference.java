package com.example.liuyaohua.cache.lruk.lru.reference;

import com.example.liuyaohua.cache.lruk.lru.CacheLoader;
import com.example.liuyaohua.cache.lruk.lru.LocalCache;
import com.example.liuyaohua.cache.lruk.lru.entry.ReferenceEntry;
import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.example.liuyaohua.cache.lruk.lru.CacheLoader;
import com.example.liuyaohua.cache.lruk.lru.LocalCache;
import com.example.liuyaohua.cache.lruk.lru.entry.ReferenceEntry;

import java.lang.ref.ReferenceQueue;
import java.util.concurrent.ExecutionException;
import java.util.function.BiFunction;

import static com.google.common.util.concurrent.Futures.transform;
import static com.google.common.util.concurrent.MoreExecutors.directExecutor;
import static com.google.common.util.concurrent.Uninterruptibles.getUninterruptibly;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-26 00:17
 * 说明 ...
 */
public class LoadingValueReference<K, V> implements ValueReference<K, V> {
    volatile ValueReference<K, V> oldValue;

    // TODO(fry): rename get, then extend AbstractFuture instead of containing SettableFuture
    final SettableFuture<V> futureValue = SettableFuture.create();
    final Stopwatch stopwatch = Stopwatch.createUnstarted();

    public LoadingValueReference() {
        this(null);
    }

    public LoadingValueReference(ValueReference<K, V> oldValue) {
        this.oldValue = (oldValue == null) ? LocalCache.<K, V>unset() : oldValue;
    }

    @Override
    public boolean isLoading() {
        return true;
    }

    @Override
    public boolean isActive() {
        return oldValue.isActive();
    }

    @Override
    public int getWeight() {
        return oldValue.getWeight();
    }

    public boolean set(V newValue) {
        return futureValue.set(newValue);
    }

    public boolean setException(Throwable t) {
        return futureValue.setException(t);
    }

    private ListenableFuture<V> fullyFailedFuture(Throwable t) {
        return Futures.immediateFailedFuture(t);
    }

    @Override
    public void notifyNewValue(V newValue) {
        if (newValue != null) {
            // The pending load was clobbered by a manual write.
            // Unblock all pending gets, and have them return the new value.
            set(newValue);
        } else {
            // The pending load was removed. Delay notifications until loading completes.
            oldValue = LocalCache.unset();
        }

        // TODO(fry): could also cancel loading if we had a handle on its future
    }

    public ListenableFuture<V> loadFuture(K key, CacheLoader<? super K, V> loader) {
        try {
            stopwatch.start();
            V previousValue = oldValue.get();
            if (previousValue == null) {            // 前面没有值，数据中当前index为空时
                V newValue = loader.load(key);      // 导入
                return set(newValue) ? futureValue : Futures.immediateFuture(newValue);
            }
            ListenableFuture<V> newValue = loader.reload(key, previousValue);
            if (newValue == null) {
                return Futures.immediateFuture(null);
            }
            // To avoid a race, make sure the refreshed value is set into loadingValueReference
            // *before* returning newValue from the lruk query.
            return transform(
                    newValue,
                    new com.google.common.base.Function<V, V>() {
                        @Override
                        public V apply(V newValue) {
                            LoadingValueReference.this.set(newValue);
                            return newValue;
                        }
                    },
                    directExecutor());
        } catch (Throwable t) {
            ListenableFuture<V> result = setException(t) ? futureValue : fullyFailedFuture(t);
            if (t instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            return result;
        }
    }

    public V compute(K key, BiFunction<? super K, ? super V, ? extends V> function) {
        stopwatch.start();
        V previousValue;
        try {
            previousValue = oldValue.waitForValue();
        } catch (ExecutionException e) {
            previousValue = null;
        }
        V newValue = function.apply(key, previousValue);
        this.set(newValue);
        return newValue;
    }

    public long elapsedNanos() {
        return stopwatch.elapsed(NANOSECONDS);
    }

    @Override
    public V waitForValue() throws ExecutionException {
        return getUninterruptibly(futureValue);
    }

    @Override
    public V get() {
        return oldValue.get();
    }

    public ValueReference<K, V> getOldValue() {
        return oldValue;
    }

    @Override
    public ReferenceEntry<K, V> getEntry() {
        return null;
    }

    @Override
    public ValueReference<K, V> copyFor(ReferenceQueue<V> queue, V value, ReferenceEntry<K, V> entry) {
        return this;
    }
}
