package com.example.liuyaohua.cache.lruk.lru.entry;

import com.example.liuyaohua.cache.lruk.lru.AbstractReferenceEntry;
import com.example.liuyaohua.cache.lruk.lru.LocalCache;
import com.example.liuyaohua.cache.lruk.lru.reference.ValueReference;

/**
 * Used for strongly-referenced keys.
 */
public class StrongEntry<K, V> extends AbstractReferenceEntry<K, V> {
    final K key;

    public StrongEntry(K key, int hash, ReferenceEntry<K, V> next) {
        this.key = key;
        this.hash = hash;
        this.next = next;
    }

    @Override
    public K getKey() {
        return this.key;
    }

    // The code below is exactly the same for each entry type.

    final int hash;
    final ReferenceEntry<K, V> next;
    volatile ValueReference<K, V> valueReference = LocalCache.unset();

    @Override
    public ValueReference<K, V> getValueReference() {
        return valueReference;
    }

    @Override
    public void setValueReference(ValueReference<K, V> valueReference) {
        this.valueReference = valueReference;
    }

    @Override
    public int getHash() {
        return hash;
    }

    @Override
    public ReferenceEntry<K, V> getNext() {
        return next;
    }
}
