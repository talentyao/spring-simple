package com.example.liuyaohua.cache.lruk;

class MutableInteger {

    public MutableInteger(int key, int value) {
        this.key = key;
        this.value = value;
    }

    int key = -1;
    int value = 1;

    public int get() {
        return value;
    }

    public void add() {
        ++this.value;
    }

    public void set(int value) {
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
