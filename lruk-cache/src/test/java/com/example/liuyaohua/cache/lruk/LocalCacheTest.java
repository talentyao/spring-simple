package com.example.liuyaohua.cache.lruk;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import junit.framework.TestCase;
import org.junit.Assert;

import java.util.concurrent.TimeUnit;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-03-16 19:16
 * <p>说明 ...
 */
public class LocalCacheTest extends TestCase {

    private static LoadingCache<Integer, MutableInteger> LRU_CACHE = null;

    private static final int CACHE_KEY_SIZE = 1024;

    private static final int CACHE_TIMEOUNT = 600;

    private void initCacheIfNecessary() {
        if (LRU_CACHE == null) {
            LRU_CACHE = CacheBuilder
                    .newBuilder()
                    .maximumSize(CACHE_KEY_SIZE)
                    .expireAfterAccess(CACHE_TIMEOUNT, TimeUnit.SECONDS)
                    .concurrencyLevel(8)
                    .build(new CacheLoader<Integer, MutableInteger>() {
                        int jj = 2100000;

                        @Override
                        public MutableInteger load(Integer key) throws Exception {
                            return new MutableInteger(key, ++jj);
                        }
                    });
        }
    }

    int NUM_ITERATIONS = 210000;

    public void test() throws Exception {
        initCacheIfNecessary();
        for (int i = 110000; i < NUM_ITERATIONS; i++) {

            if (i % 5 == 0) {
                LRU_CACHE.get(i);
                LRU_CACHE.get(i);
                LRU_CACHE.get(i);
                LRU_CACHE.get(i);
                LRU_CACHE.put(i, LRU_CACHE.get(i));
            }
            MutableInteger valueWrapper = LRU_CACHE.get(i);
            Assert.assertNotNull(valueWrapper);
            Assert.assertEquals(valueWrapper.getKey(), i);
        }
    }

    /**
     * 测试缓存最大的缓存数量不能超过设置的值
     */
    public void testMaximumSize() throws Exception {
        initCacheIfNecessary();
        for (int i = 1; i <= 1027; i++) {
            //调试用
            if(i > 129){
                Assert.assertTrue(i>129);
            }
            LRU_CACHE.get(i);
            LRU_CACHE.get(i);
            LRU_CACHE.get(i);
            LRU_CACHE.get(i);
        }
        Thread.sleep(25);
        Assert.assertEquals(1024, LRU_CACHE.size());
    }

}
