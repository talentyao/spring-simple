package com.example.liuyaohua.util;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.io.Closer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2015-11-13 16:00
 * 说明 过滤器工具
 */
public class FilterUtils {

	/**
	 * 判断该reauest是否在忽略验证的URI列表中
	 *
	 * @param request           HttpServletRequest
	 * @param blackServletPaths 黑名单 URI列表
	 * @return
	 */
	public static boolean isInForbidAccessPaths(HttpServletRequest request, List<String> blackServletPaths) {
		String servletPath = request.getServletPath();
		// 处理 黑名单 URI列表
		for (String sz : blackServletPaths) {
			if (matchIgnoreServletPath(sz, servletPath)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 从字符串中分拆出url
	 *
	 * @param str
	 * @return List<String>
	 */
	public static List<String> getUrlFromStr(String str) {
		List<String> list = Lists.newLinkedList();
		if (Strings.isNullOrEmpty(str)) {
			return list;
		}
		String[] l = str.split("\n");
		if(l == null || l.length == 0){
			return list;
		}
		for (String sz : l) {
			sz = sz.trim();
			if (!Strings.isNullOrEmpty(sz)) {
				list.add(sz);
			}
		}

		return list;
	}

	/**
	 * 判断该reauest是否在忽略验证的URI列表中
	 *
	 * @param servletPath     servlet path
	 * @param ignoreCheckList 略验证的URI列表
	 * @return true 在，false 不在
	 */
	public static boolean isIgnoreCheckValid(String servletPath, List<String> ignoreCheckList) {
		// 处理忽略列表
		for (String sz : ignoreCheckList) {
			if (matchIgnoreServletPath(sz, servletPath)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取真实返回的url+参数
	 *
	 * @param url url
	 * @param req HttpServletRequest
	 * @return
	 */
	public static String getStateURL(String url, HttpServletRequest req) {

		Enumeration enu = req.getParameterNames();
		while (enu.hasMoreElements()) {
			String paraName = (String) enu.nextElement();
			if (url.indexOf("?") == -1) { //没有url传参
				url += "?" + paraName + "=" + req.getParameter(paraName);
			} else {
				url += "&" + paraName + "=" + req.getParameter(paraName);
			}
		}
		return url;
	}

	/**
	 * Return <code>true</code> if the context-relative request path matches the requirements of the specified filter
	 * mapping; otherwise, return <code>false</code>.
	 *
	 * @param testPath    URL mapping being checked
	 * @param requestPath Context-relative request path of this request
	 */
	public static boolean matchIgnoreServletPath(String testPath, String requestPath) {

		if (testPath == null)
			return (false);

		// Case 1 - Exact Match
		if (testPath.equals(requestPath))
			return (true);

		// Case 2 - Path Match ("/.../*")
		if (testPath.equals("/*"))
			return (true);
		if (testPath.endsWith("/*")) {
			if (testPath.regionMatches(0, requestPath, 0, testPath.length() - 2)) {
				if (requestPath.length() == (testPath.length() - 2)) {
					return (true);
				} else if ('/' == requestPath.charAt(testPath.length() - 2)) {
					return (true);
				}
			}
			return (false);
		}

		// Case 3 - Extension Match
		if (testPath.startsWith("*.")) {
			int slash = requestPath.lastIndexOf('/');
			int period = requestPath.lastIndexOf('.');
			if ((slash >= 0) && (period > slash) && (period != requestPath.length() - 1)
					&& ((requestPath.length() - period) == (testPath.length() - 1))) {
				return (testPath.regionMatches(2, requestPath, period + 1, testPath.length() - 2));
			}
		}

		// Case 4 - "Default" Match
		return (false); // NOTE - Not relevant for selecting filters
	}

	/**
	 * 获取相对的请求完整路径字符串:如:/room/list.htm?a=b&c=d
	 *
	 * @param request
	 * @return
	 */
	public static String getUrlInfo(HttpServletRequest request) {
		StringBuffer urlPath = request.getRequestURL();
		String params = convertParametersAsString(request);
		if (!Strings.isNullOrEmpty(params)) {
			urlPath.append("?").append(params);
		}
		return urlPath.toString();
		/*StringBuilder url = new StringBuilder("");
        if (req.getServerPort() == 80) {
            url.append(req.getScheme()).append("://").append(req.getServerName()).append(req.getContextPath()).append(req.getServletPath());
        } else {
            url.append(req.getScheme()).append("://").append(req.getServerName()).append(req.getServerPort()).append(req.getContextPath()).append(req.getServletPath());
        }
        Enumeration enu = req.getParameterNames();
        while (enu.hasMoreElements()) {
            String paraName = (String) enu.nextElement();
            if (url.indexOf("?") == -1) { // 没有url传参
                url.append("?").append(paraName).append("=").append(req.getParameter(paraName));
            } else {
                url.append("&").append(paraName).append("=").append(req.getParameter(paraName));
            }
        }
        return url.toString();*/
	}

	/**
	 * 将请求中的参数序列化成字符串
	 *
	 * @param request
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static String convertParametersAsString(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();
		Enumeration paramEnum = request.getParameterNames();
		boolean isAppend = false;
		while (paramEnum.hasMoreElements()) {
			String paramName = (String) paramEnum.nextElement();
			if (isAppend) {
				sb.append("&");
			}
			String paramValue = request.getParameter(paramName);
			sb.append(paramName + "=" + paramValue);
			isAppend = true;
		}
		return sb.toString();
	}

	/**
	 * 获取request请求body中参数
	 *
	 * @param reader
	 * @return
	 */
	public static String getBodyString(BufferedReader reader) {
		if (reader == null) {
			return "";
		}
		Closer closer = Closer.create();
		String line;
		StringBuilder builder = new StringBuilder("");
		try {
			closer.register(reader);
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (IOException e) {
			// 暂时不处理
		} finally {
			try {
				closer.close();
			} catch (Exception e) {
				// 暂时不处理
			}
		}
		return builder.toString();
	}

	public static void resContent(HttpServletResponse res, String retStatus, String retMsg) throws IOException {
		resContent(res, retStatus, retMsg, null);
	}

	public static void resContent(HttpServletResponse res, String retStatus, String retMsg, Object t) throws IOException {
		String resContentJson = JSON.toJSONString(ResponseUtil.returnFail(retStatus, retMsg, t));
		res.setContentType("text/html;charset=UTF-8");
		res.setHeader("Pragma", "No-cache");
		res.setHeader("Cache-Control", "no-cache");
		res.setDateHeader("Expires", 0);
		res.getWriter().write(resContentJson);
		res.getWriter().flush();
	}
}
