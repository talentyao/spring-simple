package com.example.liuyaohua.util.secret;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class DES {

    private static byte[] iv = {1, 2, 3, 4, 5, 6, 7, 8};

    public static String encryptDES(String encryptString, String encryptKey) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(encryptKey),"加密key不能为空！");

        IvParameterSpec zeroIv = new IvParameterSpec(iv);
        SecretKeySpec key = new SecretKeySpec(encryptKey.getBytes(), "DES");
        Cipher cipher;
        byte[] encryptedData = null;
		try {
			cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
	        encryptedData = cipher.doFinal(encryptString.getBytes());
		} catch (Exception e) {
			throw new RuntimeException(e);
		} 
        
        return Base64.encode(encryptedData);
    }

    public static String decryptDES(String decryptString, String decryptKey) {
        byte[] byteMi = Base64.decode(decryptString);
        IvParameterSpec zeroIv = new IvParameterSpec(iv);
        SecretKeySpec key = new SecretKeySpec(decryptKey.getBytes(), "DES");
        Cipher cipher;
        byte[] decryptedData = null;
		try {
			cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);
		    decryptedData = cipher.doFinal(byteMi);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} 
       
        return new String(decryptedData);
    }

}
