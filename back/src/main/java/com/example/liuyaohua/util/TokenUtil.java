package com.example.liuyaohua.util;

import com.alibaba.fastjson.JSON;
import com.example.liuyaohua.model.Token;
import com.example.liuyaohua.util.secret.AESEncrypt;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.example.liuyaohua.model.Token;
import com.example.liuyaohua.util.secret.AESEncrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 作者 yaohua.liu
 * 日期 2016-09-05 17:31
 * 说明 token工具包
 */
public class TokenUtil {

	private final static Logger LOGGER = LoggerFactory.getLogger(TokenUtil.class);

	public static String F_SPLIT = "##";
	public static String F_JOIN = "=";
	private static String TOKEN_NAME = "token";
	/**
	 * cookie有效期 30天，单位：秒
	 */
	private static int COOKIE_MAX_AGE = 30 * 24 * 60 * 60;
	private static Splitter.MapSplitter mapSplitter = Splitter.on(F_SPLIT)
			.trimResults()
			.omitEmptyStrings()
			.withKeyValueSeparator(F_JOIN);//key与value的分割符

	/**
	 * 从cookie中获取token,并校验token信息及是否过期
	 *
	 * @param req HttpServletRequest
	 * @return Token
	 */
	public static Token getTokenFromRequest(HttpServletRequest req, String tokenKey) {
		if (req == null) {
			return null;
		}
		if (Strings.isNullOrEmpty(tokenKey)) {
			LOGGER.info("获取token解密key失败,tokenKey=null");
			return null;
		}
		/*Cookie[] cookies = req.getCookies();
		if (cookies == null || cookies.length == 0) {
			return null;
		}*/

		String tokenStr = req.getHeader("X-ACCESS-TOKEN-LL");
		//LOGGER.info("token = {}",tokenStr);
		/*for (Cookie cookie : cookies) {
			if (cookie == null || Strings.isNullOrEmpty(cookie.getName()) || Strings.isNullOrEmpty(cookie.getValue())) {
				continue;
			}
			if (Objects.equals(TOKEN_NAME, cookie.getName()) && !Strings.isNullOrEmpty(cookie.getValue())) {
				tokenStr = cookie.getValue();
				break;
			}
		}*/
		if (Strings.isNullOrEmpty(tokenStr)) {
			//LOGGER.info("token不存在");
			return null;
		}

		try {
			String ts = AESEncrypt.decrypt(tokenStr, tokenKey);
			if (Strings.isNullOrEmpty(ts)) {
				//LOGGER.error("token不存在");
				return null;
			}
			if (!ts.contains(TokenUtil.F_SPLIT)) {
				LOGGER.error("token数据转换异常: token = {},解密后的token = {}", tokenKey, ts);
				return null;
			}
			return str2Token(ts);
		} catch (Exception e) {
			LOGGER.error("token解密失败:token = {},error msg = {}", tokenStr,e.getMessage(),e);
			return null;
		}
	}

	/**
	 * 删除cookie
	 *
	 * @param request  HttpServletRequest
	 * @param response HttpServletResponse
	 * @return
	 */
	/*public static void deleteToken(HttpServletRequest request, HttpServletResponse response) {
		if (request == null || response == null) {
			return;
		}
		Cookie[] cookies = request.getCookies();
		if (null == cookies || cookies.length == 0) {
			return;
		}
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals(TOKEN_NAME)) {
				cookie.setValue(null);
				cookie.setMaxAge(0);// 立即销毁cookie
				cookie.setPath("/");
				response.addCookie(cookie);
				break;
			}
		}
	}*/

	/**
	 * 把token放入cookie中
	 *
	 * @param res    HttpServletResponse
	 * @param token  token
	 * @param aesKey 加密key
	 */
	/*public static void putToken2Cookie(HttpServletResponse res, final Token token, final String aesKey) {
		String tokenStr = token2Str(token);

		Cookie cookie = new Cookie(TOKEN_NAME, AESEncrypt.encrypt(tokenStr, aesKey));
		cookie.setPath("/");
		cookie.setMaxAge(COOKIE_MAX_AGE);

		res.addCookie(cookie);
	}*/

	/**
	 * 把token转换成字符串
	 *
	 * @param token
	 * @param aesKey
	 * @return
	 */
	public static String tokenToString(final Token token, final String aesKey){
		String tokenStr = token2Str(token);
		return AESEncrypt.encrypt(tokenStr, aesKey);
	}

	/**
	 * 把str转换成map
	 *
	 * @param str
	 * @return
	 */
	public static Token str2Token(String str) {
		if (Strings.isNullOrEmpty(str)) {
			return null;
		}
		Map<String, String> map = mapSplitter.split(str);
		if (map.size() == 0) {
			return null;
		}
		Token token = new Token();
		token.setUserId(map.get("userId"));
		token.setPhoneNumber(map.get("phoneNumber"));
		token.setName(map.get("name"));
		token.setClientIdentify(map.get("clientIdentify"));
		token.setSystemPhone(map.get("systemPhone"));
		token.setSystemModel(map.get("systemModel"));
		token.setPlatformCode(map.get("platformCode"));
		token.setLng(map.get("lng"));
		token.setLat(map.get("lat"));

		try {
			token.setCreateTime(Long.parseLong(map.get("createTime")));
			token.setPeriodOfValidit(Long.parseLong(map.get("periodOfValidit")));
			//token.checkParamThrowServiceException();
			return token;
		} catch (Exception e) {
			LOGGER.warn("map转换成token出错:map = {}, errormsg = {}", JSON.toJSONString(map), e.getMessage(), e);
			return null;
		}
	}

	/**
	 * 把token转换成str
	 *
	 * @param token Token
	 * @return String
	 */
	public static String token2Str(Token token) {
		Map<String, String> map = Maps.newHashMap();
		if (token == null) {
			return "";
		}
		map.put("userId", token.getUserId());
		map.put("phoneNumber", token.getPhoneNumber());
		if(!Strings.isNullOrEmpty(token.getName())) {
			map.put("name", token.getName());                        // 第一次登录时候可能没有传，在额度预估的时候更新该值
		}
		map.put("clientIdentify", token.getClientIdentify());
		map.put("systemPhone", token.getSystemPhone());
		map.put("systemModel", token.getSystemModel());
		map.put("platformCode", token.getPlatformCode());
		map.put("lng", token.getLng());
		map.put("lat", token.getLat());
		map.put("createTime", Long.toString(token.getCreateTime()));
		map.put("periodOfValidit", Long.toString(token.getPeriodOfValidit()));
		Joiner.MapJoiner joinerMap = Joiner.on(F_SPLIT).withKeyValueSeparator(F_JOIN);//键值之间的连接符
		return joinerMap.join(map);
	}

	/**
	 * 获取测试token
	 * 仅供测试使用
	 *
	 * @return Token
	 */
	public static Token getTestToken() {
		Token token = new Token();
		token.setUserId("254443233");
		token.setPhoneNumber("17090078480");
		token.setName("刘要华");
		token.setClientIdentify("1+++++++++++");
		token.setSystemPhone("android");
		token.setSystemModel("1");
		token.setPlatformCode("yrdjk");
		token.setLng("1.2");
		token.setLat("3.5");

		return token;
		}
}
