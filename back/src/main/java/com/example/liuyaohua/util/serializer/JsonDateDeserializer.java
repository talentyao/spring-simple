package com.example.liuyaohua.util.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 作者 yaohua.liu
 * 日期 2016-12-13 11:48
 * 说明 日期反序列化工具
 */
public class JsonDateDeserializer extends JsonDeserializer<Date> {
    private Logger logger = LoggerFactory.getLogger(JsonDateDeserializer.class);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        try {
            return sdf.parse(node.textValue());
        } catch (ParseException e) {
            logger.error("ParseException:{}", e);
        }
        return null;
    }
}
