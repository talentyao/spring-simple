package com.example.liuyaohua.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 作者 yaohua.liu
 * 日期 2016-09-05 16:51
 * 说明 基本信息
 */
@ApiModel(value = "data", description = "基本信息")
public class BaseInfo implements Serializable {
	/**
	 * 用户id
	 */
	@ApiModelProperty(value = "用户id(小于或等于11位整数)", required = true, example = "12345678910")
	protected String userId ;
	/**
	 * 手机号
	 */
	@ApiModelProperty(value = "手机号", required = false, example = "13866668888")
	protected String phoneNumber ;
	/**
	 * 手机客户端唯一标示IMEI或者MAC
	 */
	@ApiModelProperty(value = "手机客户端唯一标识IMEI或者MAC", required = true, example = "A1000050D9C863")
	protected String clientIdentify = "";
	/**
	 * 手机系统版本
	 */
	@ApiModelProperty(value = "手机系统版本", required = true, example = "android")
	protected String systemPhone = "";
	/**
	 * 手机型号
	 */
	@ApiModelProperty(value = "手机型号", required = true, example = "vivo X6S A")
	protected String systemModel = "";
	/**
	 * 平台code
	 */
	@ApiModelProperty(value = "渠道码", required = true, example = "xxApp")
	protected String platformCode ;
	/**
	 * 经度
	 */
	@ApiModelProperty(value = "经度", required = true, example = "117.163893")
	protected String lng = "";
	/**
	 * 纬度
	 */
	@ApiModelProperty(value = "纬度", required = true, example = "31.808207")
	protected String lat = "";
	/**
	 * 用户名
	 */
	@ApiModelProperty(value = "用户名", required = false, example = "张三")
	protected String name ;

	public String getUserId() {
		return userId;
	}

	public BaseInfo setUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public BaseInfo setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
		return this;
	}

	public String getName() {
		return name;
	}

	public BaseInfo setName(String name) {
		this.name = name;
		return this;
	}

	public String getClientIdentify() {
		return clientIdentify;
	}

	public BaseInfo setClientIdentify(String clientIdentify) {
		this.clientIdentify = clientIdentify;
		return this;
	}

	public String getSystemPhone() {
		return systemPhone;
	}

	public BaseInfo setSystemPhone(String systemPhone) {
		this.systemPhone = systemPhone;
		return this;
	}

	public String getSystemModel() {
		return systemModel;
	}

	public BaseInfo setSystemModel(String systemModel) {
		this.systemModel = systemModel;
		return this;
	}

	public String getPlatformCode() {
		return this.platformCode;
	}

	public BaseInfo setPlatformCode(String platformCode) {
		this.platformCode = platformCode;
		return this;
	}

	public String getLng() {
		return lng;
	}

	public BaseInfo setLng(String lng) {
		this.lng = lng;
		return this;
	}

	public String getLat() {
		return lat;
	}

	public BaseInfo setLat(String lat) {
		this.lat = lat;
		return this;
	}
	private float min = 0.000001f;

	@Override
	public String toString() {
		return "BaseInfo{" +
				"userId='" + userId + '\'' +
				", phoneNumber='" + phoneNumber + '\'' +
				", clientIdentify='" + clientIdentify + '\'' +
				", systemPhone='" + systemPhone + '\'' +
				", systemModel='" + systemModel + '\'' +
				", platformCode='" + platformCode + '\'' +
				", lng='" + lng + '\'' +
				", lat='" + lat + '\'' +
				", name='" + name + '\'' +
				'}';
	}
}
