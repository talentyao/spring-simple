package com.example.liuyaohua.model.enums;

/**
 * @author qibin.xia
 */
public enum ErrorCode {
	// 数据验证错误:10000-19999
	// 业务错误：20000-29999（未知错误：20000）
	// 本地异常：30000-39999（未知错误：30000）
	// 服务异常：40000-49999（连接超时：44000；响应超时：450000）
	// 编号格式按照： 错误类别（占1位）+接口编号（占2位）+错误编号（占2位）

	// 框架
	STOP_SERVICE("-9999", "停服"),
	NEED_LOGIN("-9989", "未登录或登录信息失效，请重新登录"),
	NEED_LOGIN_2("-9988", "未登录或登录信息失效，请重新登录"),
	FORBID_ACCESS("-9979", "禁止访问"),

	// 权限相关
	NOT_OPERATING_AUTHORITY("9989", "无当前操作权限"),

	// 数据校验错误=============================================
	VALIDATE_EMAIL_FORMAT_ERROR("10001", "邮箱格式不正确"),
	VALIDATE_MOBILE_FORMAT_ERROR("10022", "很抱歉，您的手机号码格式有误，请输入正确的手机号"), // 手机号格式不正确
	VALIDATE_VERIFYCODE_NULL_ERROR("10023", "您输入的验证码有误，请重新输入"), // 验证码为空
	VALIDATE_CHANNEL_NULL_ERROR("10025", "系统故障，请稍后再试"), // 渠道为空
	VALIDATE_MOBILE_NULL_ERROR("10029", "手机号为空"), // 手机号为空
	VALIDATE_PARAMETER_ENTITY_NULL_ERROR("10030", "参数实体为空"), // 参数实体为空

	// ################通用########################
	VALIDATE_PARAMETER_NULL_ERROR("10031", "必填参数为空"), // 参数为空
	VALIDATE_LAT_LNG_ERROR("10032", "经纬度不符合要求错误"),

	VALIDATE_FEEDBACK_LENGTH_ERROR("10034", "反馈内容不能超过200字"),
	VALIDATE_BANKCARD_NULL_ERROR("10035", "您的账单信息不符合借款要求，请更换邮箱进行验证"),
	VALIDATE_GPS_CITY_IS_NULL_ERROR("10036", "定位失败,请稍后再试"), // 必填参数applyAmount为空!

	// 邮箱相关
	LOGIN_STATUS_FAILED_MSG("1301", "邮箱登录失败，请稍后再提交"), // 邮箱登录失败

	// ==================================================帐户信息==================================================
	BUSINESS_UNKNOW_ERROR("20000", "未知错误"),

	BUSINESS_ACCOUNT_NOT_EXIST_ERROR("20101", "该账户信息不存在，请联系客服解决"), // 用户不存在
	USER_ACCOUNT_ID_ERROR("21901", "请输入18位本人身份证号"),
	USER_GEOGRAPHICAL_POSITION_ERROR("20301", "对不起,未查询到您的定位信息"), // 无定位信息没有获取
	// 业务错误-流程业务错误码
	FLOWSTATUS_ERROR("20001", "对不起,您当前的流程状态不可进行此操作"), // 用户当前流程状态不可进行此操作
	// 业务错误-CommonController公用控制器业务错误码
	PROVINCES_QUERY_NULL_ERROR("20101", "当前访问过多，请稍后重试"), // 未查询到城市数据
	// 业务错误-2003表示宜人贷邮箱已经被使用
	EMAIL_EXIST_ERROR("20201", "邮箱在宜人贷系统中已存在,请更换邮箱"),
	PROVINCES_LNGANDLAT_NULL_ERROR("20102", "抱歉，未获取到您当前地理位置，请重试"),

	// ==================================================额度预估==================================================
	// 额度预估账单验证
	BUSINESS_BILL_CHECK_NO_ACCOUNT("22000", "邮箱不符合借款要求，请更换邮箱账户"), // 邮箱里没有账单
	BUSINESS_BILL_CHECK_NO_ACCOUNT_BILL("22001", "邮箱账单不足"), // 邮箱账单不足
	// 额度预估-提交额度预估
	BUSINESS_NO_EXIST_LIMIT_CONFIRM("22003", "不存在待确认的额度预估数据"), // 不存在待确认的额度预估数据
	BUSINESS_LIMIT_OUT_OF_DATE("22004", "额度预估数据已过期,不能确认"), // 额度预估数据已过期,不能确认
	// 额度预估-查询借款金额和期限
	BUSINESS_NO_EXIST_LIMIT_APPLY("22005", "用户无已确认的申请金额和期限数据"), // 用户无确认后的金额和期限数据
	// 额度预估-查询费率信息
	BUSINESS_NO_EXIST_INTERSEST("22006", "没有对应期限的费率信息"), // 没有对应期限的费率信息
	// 额度预估- 15天内预估过,则查询本地额度预估数据
	BUSINESS_NO_EXIST_LIMIT("22007", "本地DB不存在该用户的有效额度预估数据"), // 本地DB不存在该用户的有效额度预估数据
	// 额度预估-提交额度预估-非法的申请金额或期限
	BUSINESS_INVALIDATE_LIMIT_VALUE("22008", "非法的申请金额或期限"), // 非法的申请金额或期限
	// 额度预估-保存借款申请接口需要的扩展数据失败
	BUSINESS_LIMIT_SAVE_APPLY_EXTRA_INFO_TO_DB_ERROR("22009", "向DB中保存tb_user_info信息失败"), // 向DB中保存tb_user_info信息失败
	// 额度预估-提交额度预估-非法的申请金额或期限
	BUSINESS_INVALIDATE_HTML_API_DATA_PARAM_NULL_ERROR("22009", "非法的标准化API结果数据,缺少必要参数"), // 非法的标准化API结果数据,缺少必要参数
	// 额度预估-提交额度预估-获取的userId或name与登录用户不一致
	BUSINESS_INVALIDATE_HTML_API_DATA_USER_ERROR("22010", "非法的标准化API结果数据,获取的用户ID或name与登录用户不一致"), // 非法的标准化API结果数据,获取的用户ID或name与登录用户不一致
	// 额度预估-非法操作
	BUSINESS_INVALIDATE_CREDITLIMIT_OPERATE("22011", "非法的额度预估操作"), // 非法的额度预估操作
	REGISTER_USER_EXISTS("22012", "该用户已注册，请进行其他操作"), // 该用户已注册，请进行其他操作
	BUSINESS_EXIST_YRD_LENDING_USER("22013", "您的身份是宜人贷的出借人，暂不能进行借款。有问题请联系客服"), // 宜人贷出借用户不能在该渠道申请急速模式
	BUSINESS_EXIST_APPLY_STATUS("22015", "您处于锁定期内，请登录宜人贷借款APP查看"), // 借款状态-用户有借款状态
	BUSINESS_USER_REGIST_LESS_NINTY("22016", "请到宜人贷APP完成借款"), // 用户信息-在宜人贷注册时间小于90天
	BUSINESS_USER_REGIST_OLD_USER("22017", "宜人贷老用户"), // 是立即申请宜人贷老用户
	BUSINESS_USER_PHONE_NO_ERROR("22018", "手机号错误"), // 登录接口手机号输入错误
	BUSINESS_USER_REGISTER_OLD_USER("22020", "宜人贷老用户"), // 是注册的宜人贷老用户
	BUSINESS_BILL_EBANK_NO_ACCOUNT("22021", "邮箱里没有账单，你可尝试手动更新账单"), // 网银验证里没有账单
	BUSINESS_BILL_EBANK_NO_ACCOUNT_BILL("22022", "您的账单信息不符合借款要求，您可以选择换一个信用卡账单邮箱进行额度申请"), // 网银验证账单不足
	LOCAL_CHECK_IMAGE_MSG_ERROR("22026", "您输入的图片验证码不正确"),
	LOCAL_USER_NOT_EXIST("22027", "用户不存在！"),
	REGISTER_USER_CODE("22030", "验证码错误"), // 注册验证码错误
	UPDATE_AMOUNT_CODE("22031", "更改申请额度大于最高额度"), // 用户的修改额度大于申请额度
	UPDATE_TERM_CODE("22032", "更改申请期限大于最高申请期限"), // 用户的修改额度大于申请额度

	// 本地异常====================================================
	LOCAL_CITY_NOT_SUPPORT("30202", "当前城市不支持"),
	LOCAL_PHOTO_NOT_EXIST("35001", "图片数据不存在"),
	LOCAL_PHOTO_TYPE_ERROR("35002", "所传文件不是图片"),
	LOCAL_GET_PHOTO_ERROR("35003", "处理图片数据出现异常"),

	// =================================================银联/银行/网银==================================================
	LOCAL_GET_BANK_LIST_ERROR("30399", "服务器繁忙，请稍候再试"), // 获取银行列表失败

	BANK_CARD_INFO_NOT_EXIST_VALID_FOR_ALL("30400", "所有银行卡都不存在支持银联实名验证"),
	BANK_CARD_INFO_VALID_FAILED("30401", "银行卡信息验不证通过"),

	EBANK_GET_NAME_CODE("30402", "您的账单信息不符合借款要求"),

	// ==================================================OpenK接口==================================================
	LOCAL_OPENK_API_INVOKING_ERROR("38000", "调用OPENK API异常"), // 调用OPENK API异常
	LOCAL_OPENK_API_RETURN_ERROR("38001", "OPENK API返回错误信息"), // 调用OPENK API返回错误码
	LOCAL_OPENK_RESULT_NOT_MATCH_RECENT_APPLY_INFO_CODE("38002", "recentApplyInfo.getCode 与 openk 结果不匹配"),
	LOCAL_OPENK_API_RETURN_UNKNOWN_ERROR("38003", "OPENK API返回未知错误"),

	// ==================================================卡牛接口==================================================
	LOCAL_KN_API_FAIL_BALL_SOURCE_USER("39007", "调用卡牛API获取用户不是银联用户"),
	//	LOCAL_KN_API_SYSTEM_ERROR("39000", "调用卡牛API异常"), // 调用卡牛API异常
	LOCAL_KN_API_FAIL_K_N_USER_QUOTA("39001", "调用卡牛API获取模型参数失败"), // 调用卡牛API获取模型参数失败
	LOCAL_KN_USER_ID_encrypt_ERROR("39002", "卡牛user id加密失败"),
	// 卡牛获取设备及GPS信息接口设备信息为空
	KN_CLIENT_API_VALIDATE_DEVICE_NULL_ERROR("39003", "无法获取终端信息"), // 未能获取到设备信息
	// 卡牛获取设备及GPS信息接口GPS信息为空
	KN_CLIENT_API_VALIDATE_GPS_NULL_ERROR("39004", "无法获取位置信息"), // 未能获取到设备信息

	// ==================================================挖财接口==================================================
	LOCAL_WC_API_FAIL_GET_USER_INFO("39501", "调用挖财API获取 用户信息 失败"),
	LOCAL_WC_API_FAIL_GET_CARD_INFO("39502", "调用挖财API获取 银行卡及帐单信息 失败"),
	LOCAL_WC_BANK_BILL_INFO_ERROR("39503", "挖财获取银行卡及帐单信息出错"),

	// ==================================================信用管家接口==================================================
	LOCAL_XYGJ_PBOC_DATA_FAIL("38101", "pboc数据不存在或解析失败"), //征信数据过期|征信字段缺失|（信用卡）帐户数不正确|（信用卡）贷款数数不正确

	// 服务异常====================================================
	SERVICE_SERVER_UNKNOW_ERROR("40000", "系统错误,请稍后重试"),
	OUTER_API_HTTP_ERROR("46000", "外部API调用失败"),
	OUTER_HTTP_REQUEST_TIME_OUT_ERROR("46001", "外部API调用超时"),

	// =================================================="数据库==================================================
	SERVICE_DB_ERROR("47000", "数据库操作失败"),;

	private String code;

	private String message;

	ErrorCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static String getMsgByCode(String code) {
		ErrorCode[] errorCodes = ErrorCode.values();
		for (ErrorCode errorCode : errorCodes) {
			if (errorCode.getCode().equals(code)) {
				return errorCode.getMessage();
			}
		}
		return null;
	}

	public static long getCode(ErrorCode errorCode) {
		if (errorCode == null) {
			return 4000;
		}
		try {
			return Long.parseLong(errorCode.getCode());
		} catch (Exception e) {
			return 4000;
		}
	}

}
