package com.example.liuyaohua.core.filter;

import com.example.liuyaohua.model.SystemStopServiceProfile;
import com.example.liuyaohua.model.enums.ErrorCode;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.example.liuyaohua.model.SystemStopServiceProfile;
import com.example.liuyaohua.model.enums.ErrorCode;
import com.example.liuyaohua.util.FilterUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2016-12-13 11:48
 * 说明 停服
 */
public class StopServiceFilter implements Filter {

//	@Autowired
	//private ISystemStopService iSystemStopService;

	// 白名单 对所有过滤器有效
	private final List<String> ignoreCheckServletPaths = Lists.newArrayList();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// 加载白名单
		String ignoreCheckServletPathStr = filterConfig.getInitParameter("ignoreCheckServletPaths");
		ignoreCheckServletPaths.addAll(FilterUtils.getUrlFromStr(ignoreCheckServletPathStr));
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		String servletPath = req.getServletPath();
		// 白名单 对所有过滤器有效
		if (FilterUtils.isIgnoreCheckValid(servletPath, ignoreCheckServletPaths)) {
			chain.doFilter(request, response);
			return;
		}

		//Map<String, String[]> params = request.getParameterMap();
		String platformCode = "*";//getIndex0(params.get("platformCode"));
		SystemStopServiceProfile systemStopServiceProfile = null;//iSystemStopService.getProfile(platformCode);
		if (systemStopServiceProfile != null
				&& !Strings.isNullOrEmpty(systemStopServiceProfile.getPlatformCode())
				&& systemStopServiceProfile.getStartTime() != null
				&& systemStopServiceProfile.getEndTime() != null) {
			FilterUtils.resContent(res, ErrorCode.STOP_SERVICE.getCode(), ErrorCode.STOP_SERVICE.getMessage(), systemStopServiceProfile);
			return;
		}
		chain.doFilter(req, res);
	}

	/**
	 * 从数组中获取第一个元素
	 *
	 * @param array 数组
	 * @return 不存在时返回‘*’
	 */
	private String getIndex0(String[] array) {
		if (array == null || array.length == 0) {
			return "*";
		}
		if (Strings.isNullOrEmpty(array[0])) {
			return "*";
		}
		return array[0];
	}

	@Override
	public void destroy() {

	}
}
