package com.example.liuyaohua.nio;

import java.nio.file.*;
import java.util.List;

/**
 * 作者： yaohua.liu
 * 日期： 2017-07-18
 * 描述： ...
 */
public class Watcher {
	public static void main(String[] args) {
		Path this_dir = Paths.get("/home/liuyaohua/tools/code/java/Root/Old/spring-simple/target/test-classes/");
		System.out.println("Now watching the current directory...");

		try {
			WatchService watcher = this_dir.getFileSystem().newWatchService();
			this_dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE);

			WatchKey watckKey = watcher.take();

			List<WatchEvent<?>> events = watckKey.pollEvents();
			for (WatchEvent event : events) {
				System.out.println("Someone just created the file '" + event.context().toString() + "'.");

			}

		} catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
	}
}
