package com.example.liuyaohua.Interview;

import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2017-05-27 12:13
 * 说明 ...
 */
public class TryCatchTest {

	@Test
	public void testTryCatch(){
		System.out.println("return is "+test());
//		this is 1
//		this is 3
//		return is 3
	}

	private int test(){
		try {
			System.out.println("this is 1");
			throw new IllegalAccessException();
//			return 1;
		}catch (Exception e){
			System.out.println("this is 2");
			return 2;
		}finally {
			System.out.println("this is 3");
			return 3;
		}
	}
}
