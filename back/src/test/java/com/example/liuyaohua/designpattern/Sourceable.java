package com.example.liuyaohua.designpattern;

/**
 * 作者 yaohua.liu
 * 日期 2017-05-31 18:21
 * 说明 ...
 */
public interface Sourceable {
	public void method();
}
