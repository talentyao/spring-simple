package com.example.liuyaohua.guava.base;

import com.google.common.base.Throwables;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2014-10-17 19:48
 *
 * 说明 ...
 */
public class ThrowablesTest {

	@Test(expected = java.lang.IllegalStateException.class)
	public void testThrowables() {

		try {
			throw new IllegalStateException("xxxx");
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
	}
}
