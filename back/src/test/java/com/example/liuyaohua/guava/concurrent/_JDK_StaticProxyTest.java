package com.example.liuyaohua.guava.concurrent;

//代理角色：

import org.junit.Test;

/**
 * 作者 yaohua.liu
 * <p/>
 * 日期 2014-11-27 17:44
 * <p/>
 * * <p/>
 * 说明 静态代理
  */
public class _JDK_StaticProxyTest {
    //抽象角色：
    private abstract class Subject {
        abstract public void request();
    }

    //真实角色：实现了Subject的request()方法。
    private class RealSubject extends Subject {
        public RealSubject() {
        }

        public void request() {
            System.out.println("From real subject.");
        }
    }

    //代理角色：
    private class ProxySubject extends Subject {
        private Subject realSubject; //以真实角色作为代理角色的属性

        public ProxySubject() {
        }

        public ProxySubject(Subject subject) {
            this.realSubject=subject;
        }

        public void request() { //该方法封装了真实对象的request方法
            preRequest(); //something you want to do before requesting
            if (realSubject == null) {
                realSubject = new RealSubject();
            }
            realSubject.request(); //此处执行真实对象的request方法
            postRequest();//something you want to do after requesting
        }

        private void preRequest() {
            //something you want to do before requesting
        }

        private void postRequest() {
            //something you want to do after requesting
        }
    }

    @Test
    public void test(){
        Subject sub=new ProxySubject(new RealSubject());
        //Subject sub=new ProxySubject();
        sub.request();
    }

}
