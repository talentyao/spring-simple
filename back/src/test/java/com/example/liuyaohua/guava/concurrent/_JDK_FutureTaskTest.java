package com.example.liuyaohua.guava.concurrent;

import org.junit.Test;

import java.util.concurrent.*;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-26 17:37
 * * 说明 ...
 */
public class _JDK_FutureTaskTest {

    @Test
    public void testFutureTask() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        FutureTask<String> futureTask = new FutureTask<String>(
                new Callable<String>() {//使用Callable接口作为构造参数
                    public String call() {
                        //真正的任务在这里执行，这里的返回值类型为String，可以为任意类型
                        return "null";
                    }
                }
        );
        executor.execute(futureTask);

        String result = "";
        //在这里可以做别的任何事情
        try {
            //取得结果，同时设置超时执行时间为5秒。同样可以用future.get()，不设置执行超时时间取得结果
            result = futureTask.get(5000, TimeUnit.MILLISECONDS);
            System.out.println(result);
        } catch (InterruptedException e) {
            futureTask.cancel(true);
        } catch (ExecutionException e) {
            futureTask.cancel(true);
        } catch (TimeoutException e) {
            futureTask.cancel(true);
        } finally {
            executor.shutdown();
        }
    }
}
