package com.example.liuyaohua.guava.collect;

import com.google.common.collect.Lists;
import com.google.common.collect.ObjectArrays;
import org.junit.Test;

import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-21 16:26
 * * 说明 ...
 */
public class ObjectArraysTest {
    @Test
    public void testObjectArrays() {
        // 声明一个数组 第一个参数是元素类型，第二个参数是数组长度
        String[] arr = ObjectArrays.newArray(String.class, 10);
        // 新生成一个空数组，其类型跟第一个参数的元素类型一样，第二个参数是数组长度
        String[] arr0 = ObjectArrays.newArray(arr, 1);

        String[] arr1 = new String[] { "a", };
        String[] arr2 = new String[] { "b", "c" };
        // 合并两个数组，第三个参数是新数组元素的类型
        String[] arr3 = ObjectArrays.concat(arr1, arr2, String.class);

        // list转换成数组
        List<String> list = Lists.newArrayList(Lists.asList("0", arr3));
        Object[] test = list.toArray();


    }
}
