package com.example.liuyaohua.guava.concurrent;

import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-27 11:58
 * * 说明 ...
 */
public class _JDK_ScheduledExecutorServiceTest {

    @Test
    public void testScheduledExecutorService() throws InterruptedException {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);
        Runnable task = new Runnable() {
            public void run() {
                System.out.println("beep:" + System.currentTimeMillis());
            }
        };
        final ScheduledFuture<?> beeperHandle = scheduler.scheduleAtFixedRate(task, 1, 2, TimeUnit.SECONDS);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean done = beeperHandle.cancel(true);
        System.out.println("是否关闭任务？"+done);

    }
}
