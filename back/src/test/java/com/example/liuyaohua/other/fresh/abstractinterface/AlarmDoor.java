package com.example.liuyaohua.other.fresh.abstractinterface;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-21 19:13
 * 包名 com.qunar.fresh.abstractinterface
 * 说明 ...
 */
public class AlarmDoor extends AbstractAlarmDoor implements Alarm{

    @Override
    public void openDoor() {
        setDoorStatus(true);
    }

    @Override
    public void closeDoor() {
        setDoorStatus(false);
    }

    @Override
    public void alarm() {
        System.out.println("I want to alarm");
    }

    @Override
    public void setDoorStatus(boolean doorStatus) {
        this.doorStatus=doorStatus;
    }

    @Override
    public boolean getDoorStatus() {
        return doorStatus;
    }
}
