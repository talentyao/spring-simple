package com.example.liuyaohua.other.geohash;

/**
 * 作者 yaohua.liu
 * 日期 2015-09-18 15:01
 * 包名 geohash
 * 说明 ...
 */
import java.util.BitSet;
import java.util.HashMap;


public class Geohash {

    private static int numbits = 6 * 5  ;
    final static char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

    final static HashMap<Character, Integer> lookup = new HashMap<Character, Integer>();
    static {
        int i = 0;
        for (char c : digits)
            lookup.put(c, i++);
    }

    static double lon = 39.92321;
    static double lan = 116.39053344726562;
    public static void main(String[] args)  throws Exception{


        System.out.println(new Geohash().encode(lon, lan));

        double d8[] = new Geohash().decode("wx4g0ec1");
        double d7[] = new Geohash().decode("wx4g0ec");
        double d6[] = new Geohash().decode("wx4g0e");
        double d5[] = new Geohash().decode("wx4g0");
        double d4[] = new Geohash().decode("wx4g");

        System.out.println("=8");
        System.out.println(d8[0]+","+d8[1]);
        System.out.println("x8 = "+(d8[0]-lon)*100000);

        System.out.println("=7");
        System.out.println(d7[0]+","+d7[1]);
        System.out.println("x7 = "+(d7[0]-lon)*100000);

        System.out.println("=6");
        System.out.println(d6[0]+","+d6[1]);
        System.out.println("x6 = "+(d6[0]-lon)*100000);

        System.out.println("=5");
        System.out.println(d5[0]+","+d5[1]);
        System.out.println("x5 = "+(d5[0]-lon)*100000);

        System.out.println("=4");
        System.out.println(d4[0]+","+d4[1]);
        System.out.println("x4 = "+(d4[0]-lon)*100000);

        /**
         wx4g0ec181b0
         =8
         39.92311477661133,116.39053344726562
         x8 = -9.522338866929658
         =7
         39.922943115234375,116.39053344726562
         x7 = -26.68847656224216
         =6
         39.9188232421875,116.38916015625
         x6 = -438.67578124974216
         =5
         39.90234375,116.3671875
         x5 = -2086.624999999742
         =4
         39.90234375,116.3671875
         x4 = -2086.624999999742
         */

    }

    public double[] decode(String geohash) {
        StringBuilder buffer = new StringBuilder();
        for (char c : geohash.toCharArray()) {

            int i = lookup.get(c) + 32;
            buffer.append( Integer.toString(i, 2).substring(1) );
        }

        BitSet lonset = new BitSet();
        BitSet latset = new BitSet();

        //even bits
        int j =0;
        for (int i=0; i< numbits*2;i+=2) {
            boolean isSet = false;
            if ( i < buffer.length() )
                isSet = buffer.charAt(i) == '1';
            lonset.set(j++, isSet);
        }

        //odd bits
        j=0;
        for (int i=1; i< numbits*2;i+=2) {
            boolean isSet = false;
            if ( i < buffer.length() )
                isSet = buffer.charAt(i) == '1';
            latset.set(j++, isSet);
        }

        double lon = decode(lonset, -180, 180);
        double lat = decode(latset, -90, 90);

        return new double[] {lat, lon};
    }

    private double decode(BitSet bs, double floor, double ceiling) {
        double mid = 0;
        for (int i=0; i<bs.length(); i++) {
            mid = (floor + ceiling) / 2;
            if (bs.get(i))
                floor = mid;
            else
                ceiling = mid;
        }
        return mid;
    }


    public String encode(double lat, double lon) {
        BitSet latbits = getBits(lat, -90, 90);
        BitSet lonbits = getBits(lon, -180, 180);
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < numbits; i++) {
            buffer.append( (lonbits.get(i))?'1':'0');
            buffer.append( (latbits.get(i))?'1':'0');
        }
        return base32(Long.parseLong(buffer.toString(), 2));
    }

    private BitSet getBits(double lat, double floor, double ceiling) {
        BitSet buffer = new BitSet(numbits);
        for (int i = 0; i < numbits; i++) {
            double mid = (floor + ceiling) / 2;
            if (lat >= mid) {
                buffer.set(i);
                floor = mid;
            } else {
                ceiling = mid;
            }
        }
        return buffer;
    }

    public static String base32(long i) {
        char[] buf = new char[65];
        int charPos = 64;
        boolean negative = (i < 0);
        if (!negative)
            i = -i;
        while (i <= -32) {
            buf[charPos--] = digits[(int) (-(i % 32))];
            i /= 32;
        }
        buf[charPos] = digits[(int) (-i)];

        if (negative)
            buf[--charPos] = '-';
        return new String(buf, charPos, (65 - charPos));
    }

}