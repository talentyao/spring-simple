package com.example.liuyaohua.util;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

public class XSSUtil {
	public final static List<String> xssLi = new ArrayList<String>();
	static{
		xssLi.add("&lt;");
		xssLi.add("&gt;");
		xssLi.add(">");
		xssLi.add("<");
		xssLi.add("&#60;");
		xssLi.add("&#62;");
		xssLi.add("script");
		xssLi.add("submit");
		xssLi.add("iframe");
		xssLi.add("link");
		xssLi.add("src");
		xssLi.add("href");
		xssLi.add("onload");
		xssLi.add("include");
		xssLi.add("style");
		xssLi.add("document");
		xssLi.add("onclick");
		xssLi.add("alert");
		xssLi.add("open");
	}

	/**
	 * 过滤特殊字符，防止xss攻击
	 *
	 * @param content
	 * @return
	 */
	public static String cleanXSS(String content){
		if (Strings.isNullOrEmpty(content)) {
			return "";
		}

		//大写字母转为小写
		//content = content.toLowerCase();
		//检索是否包含敏感字符
		boolean isSafe = false;
		for(int i=0;i<xssLi.size();i++){
			if(content.contains(xssLi.get(i))){
				isSafe = true;
			}
		}
		//包含直接过率，否则返回
		if(isSafe){
			for(int i=0;i<xssLi.size();i++){
				content = content.replace(xssLi.get(i),"");
			}
			//再次回调
			content = cleanXSS(content);
		}
		return content;
	}

	/**
	 * 过滤特殊字符，防止xss攻击
	 *
	 * @param value
	 * @return
	 */
	/*private String cleanXSS(String value) {
		if (Strings.isNullOrEmpty(value)) {
			return "";
		}
		value = value.replaceAll("<", "& lt;").replaceAll(">", "& gt;");
		value = value.replaceAll("\\(", "& #40;").replaceAll("\\)", "&#41;");
		value = value.replaceAll("'", "& #39;");
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		value = value.replaceAll("script", "");
		return value;
	}*/
}
