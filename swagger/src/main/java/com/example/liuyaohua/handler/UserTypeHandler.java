package com.example.liuyaohua.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.liuyaohua.model.UserStatusEnum;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import com.example.liuyaohua.model.UserStatusEnum;


public class UserTypeHandler implements TypeHandler<UserStatusEnum> {
	
	@Override
	public UserStatusEnum getResult(ResultSet rs, String columnName)
			throws SQLException {
		return UserStatusEnum.codeOf(rs.getInt(columnName));
	}

	@Override
	public UserStatusEnum getResult(ResultSet rs, int columnIndex)
			throws SQLException {
		return UserStatusEnum.codeOf(rs.getInt(columnIndex));
	}

	@Override
	public UserStatusEnum getResult(CallableStatement cs, int columnIndex)
			throws SQLException {
		return UserStatusEnum.codeOf(cs.getInt(columnIndex));
	}

	@Override
	public void setParameter(PreparedStatement ps, int i,
			UserStatusEnum parameter, JdbcType arg3) throws SQLException {
		ps.setInt(i, parameter.getCode());
	}

}
