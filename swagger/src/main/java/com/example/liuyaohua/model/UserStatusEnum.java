package com.example.liuyaohua.model;


public enum UserStatusEnum {
	NORMAL(0, "正常"), BLOCK(1, "注销"), DELETE(2, "删除");
	private int code;
	private String des;

	UserStatusEnum(int code, String des) {
		this.code = new Integer(code);
		this.des = des;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public static UserStatusEnum codeOf(int code) {
		for (UserStatusEnum status : values()) {
			if (status.code == code) {
				return status;
			}
		}

		throw new IllegalArgumentException("Invalid role code: " + code);
	}
}
