package com.example.liuyaohua;

import com.example.liuyaohua.dao.UserDao;
import com.example.liuyaohua.model.User;
import com.example.liuyaohua.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.annotation.Resource;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-07-24 18:27
 * <p>说明 ...
 */
public class MockitoTest extends CommonTest {

    @InjectMocks    // mock注入说明：会注入变量
    @Resource
    private UserService userService;

    @Mock           // mock一个实例，注入到上面声明的 UserService 中
    @Resource
    private UserDao userDao;

    @Rule           // 对异常代码进行mock声明
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        // mock注解声明及初始化
        MockitoAnnotations.initMocks(this);

        // 针对 @Mock 类中的方法进行定制：当调用该接口时返回固定值
        when(userDao.saveUser(any(User.class))).thenReturn(1314);
        when(userDao.updateUserById(any(User.class))).thenReturn(111);
    }

    @Test
    public void testMock() {
        // 调用前面指定的mock接口
        int result = userService.saveUser(new User());
        // 检验返回的固定值
        Assert.assertEquals(1314, result);

        // 调用前面指定的mock接口
        result = userService.updateUserById(new User());
        // 检验返回的固定值
        Assert.assertEquals(111, result);
    }

    @Test
    public void testThrown() {
        // 检验异常类
        thrown.expect(IllegalStateException.class);
        // 检验异常信息
        thrown.expectMessage(UserService.ERROR_MSG);
        // 开始执行方法
        userService.testException();
    }
}
