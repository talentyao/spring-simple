package com.example.liuyaohua.dao;

import org.springframework.stereotype.Repository;
import com.example.liuyaohua.model.User;

import java.util.List;

@Repository
public interface UserDao {

    int saveUser(User user);

    User queryUserById(int id);

    List<User> listUser();

    int updateUserById(User user);

    int deleteUserById(int id);
}
