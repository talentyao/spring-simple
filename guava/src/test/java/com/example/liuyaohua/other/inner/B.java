package com.example.liuyaohua.other.inner;

/**
 * 作者 yaohua.liu
 * 日期 2015-09-07 15:06
 *
 * 说明 B、接口式的匿名内部类
 */
public class B {
    public static void main(String[] args) {
        Vehicle v = new Vehicle(){
            public void drive(){
                System.out.println("Driving a car!");
            }
        };
        v.drive();
    }
}
