package com.example.liuyaohua.other.fresh.abstractinterface;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-21 19:10
 * 包名 com.qunar.fresh.abstractinterface
 * 说明 ...
 */
public abstract class AbstractAlarmDoor extends AbstractDoor implements Alarm{

/*    public abstract void openDoor();

    public abstract void closeDoor();*/

    protected boolean doorStatus = false;

    public abstract void setDoorStatus(boolean status);

    public abstract boolean getDoorStatus();
}
