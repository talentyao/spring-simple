package com.example.liuyaohua.other;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.io.BaseEncoding;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2014-09-26 18:34
 * * 说明 ...
 */
public class BaseEncodingTest {

    @Test
    public void testEncode(){
        java.lang.String s=BaseEncoding.base32().encode("foo".getBytes(Charsets.UTF_8));
        System.out.println("源：foo\n加密后："+s);

        byte[]result=BaseEncoding.base32().decode(s);
        System.out.println("源：foo\n加密后再解密："+new java.lang.String(result));
    }

    @Test
    public void testPreconditions(){
        try {
            Preconditions.checkArgument(1 <= 0, "房屋Id(%s)非法",1);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
