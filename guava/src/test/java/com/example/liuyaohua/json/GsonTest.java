package com.example.liuyaohua.json;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2016-06-12 14:03
 * 说明 ...
 */
public class GsonTest {
    People p;

    @Before
    public void init() {
        p = new People();
        p.setAge(20);
        p.setName("People");
        p.setSetName(true);
        p.setAddressName("xxx");
    }

    @Test
    public void test1() {
        Gson gson = new Gson();
        String json = gson.toJson(p);
        System.out.println(json);

        People newPeople = gson.fromJson(json, People.class);
        System.out.println(newPeople);

        Assert.assertNotNull(newPeople);
        Assert.assertEquals(newPeople.getAge(),p.getAge());
        Assert.assertEquals(newPeople.getName(),p.getName());
        Assert.assertEquals(newPeople.getAddressName(),p.getAddressName());
    }

    @Test
    public void test2() {
        ExclusionStrategy excludeStrategy = new SetterExclusionStrategy();
        Gson gson1 = new GsonBuilder()
                .setExclusionStrategies(excludeStrategy)
                .create();

        String json1 = gson1.toJson(p);
        System.out.println(json1);

        People newPeople = gson1.fromJson(json1, People.class);
        System.out.println(newPeople);

        Assert.assertNotNull(newPeople);
        Assert.assertEquals(newPeople.getAge(),p.getAge());
        Assert.assertEquals(newPeople.getName(),p.getName());
        Assert.assertEquals(newPeople.getAddressName(),p.getAddressName());
    }
}

class SetterExclusionStrategy implements ExclusionStrategy {
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }

    public boolean shouldSkipField(FieldAttributes f) {
        return f.getName().startsWith("set");
    }
}

class People {
    @Expose(serialize = false, deserialize = false)
    String name;
    @Expose
    @SerializedName("aaa")
    int age;
    boolean setName = false;
    @SerializedName("address_name")
    String addressName = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getSetName() {
        return setName;
    }

    public void setSetName(boolean setName) {
        this.setName = setName;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    @Override
    public String toString() {
        return "People{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", setName=" + setName +
                ", addressName='" + addressName + '\'' +
                '}';
    }
}

