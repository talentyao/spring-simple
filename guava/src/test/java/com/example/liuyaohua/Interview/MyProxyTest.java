package com.example.liuyaohua.Interview;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-27 17:14
 * 说明 Proxy（代理模式）
 * 代理模式的作用是：为其他对象提供一种代理以控制对这个对象的访问。在某些情况下，一个客户不想或者不能直接引用另一个对象，而代理对象可以在客户端和目标对象之间起到中介的作用。
 * 代理模式一般涉及到的角色有：
 * 抽象角色：声明真实对象和代理对象的共同接口；
 * 代理角色：代理对象角色内部含有对真实对象的引用，从而可以操作真实对象，同时代理对象提供与真实对象相同的接口以便在任何时刻都能代替真实对象。同时，代理对象可以在执行真实对象操作时，附加其他的操作，相当于对真实对象进行封装。
 * 真实角色：代理角色所代表的真实对象，是我们最终要引用的对象。
 */
public class MyProxyTest {

	//抽象角色：
	private interface Subject {
		void request() throws InterruptedException;
	}

	//真实角色：实现了Subject的request()方法。
	private class RealSubject implements Subject {
		public RealSubject() {
		}

		public void request() throws InterruptedException {
			Thread.sleep(1000);
			System.out.println("From real subject.");
		}
	}

	//代理角色：
	private class ProxySubject extends RealSubject {
		private RealSubject realSubject; //以真实角色作为代理角色的属性

		public ProxySubject() {
		}

		public void request() throws InterruptedException { //该方法封装了真实对象的request方法
			preRequest(); //something you want to do before requesting
			if (realSubject == null) {
				realSubject = new RealSubject();
			}
			realSubject.request(); //此处执行真实对象的request方法
			postRequest();//something you want to do after requesting
		}

		private void preRequest() {
			//something you want to do before requesting
		}

		private void postRequest() {
			//something you want to do after requesting
		}
	}

	@Test
	public void testProxy() throws InterruptedException {
		Subject sub = new ProxySubject();
		sub.request();
	}

	@Test
	public void test() {
		TimeLimiter limiter = new SimpleTimeLimiter();
		Subject sub = new ProxySubject();
		Subject proxy = limiter.newProxy(sub, Subject.class, 1000, TimeUnit.MILLISECONDS);
		try {
			proxy.request();
		} catch (Exception e) {
			System.out.println("TimeLimiter timeout!!!");
		}
	}
}
