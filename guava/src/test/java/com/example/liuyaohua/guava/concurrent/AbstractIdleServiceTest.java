package com.example.liuyaohua.guava.concurrent;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-26 12:16
 * * 说明 AbstractIdleService 类简单实现了Service接口、其在running状态时不会执行任何动作–因此在running时也不需要启动线程–但需要处理开启/关闭动作
 * 要实现一个此类的服务，只需继承AbstractIdleService类，然后自己实现startUp() 和shutDown()方法就可以了。
 */
import com.google.common.util.concurrent.AbstractIdleService;
import com.google.common.util.concurrent.Service;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests for {@link AbstractIdleService}.
 *
 * @author Chris Nokleberg
 * @author Ben Yu
 */
public class AbstractIdleServiceTest {


    private static class DefaultService extends AbstractIdleService {
        @Override
        protected void startUp() throws Exception {
            System.out.println("startUp");
        }

        @Override
        protected void shutDown() throws Exception {
            System.out.println("shutDown");
        }
    }

    @Test
    public void testServiceStart_Stop() throws Exception {
        AbstractIdleService service = new DefaultService();
        service.startAsync().awaitRunning();
        assertEquals(Service.State.RUNNING, service.state());
        service.stopAsync().awaitTerminated();
        assertEquals(Service.State.TERMINATED, service.state());
    }

    @Test
    public void testStart_failed() throws Exception {
        AbstractIdleService service = new DefaultService() {
            @Override
            protected void startUp() throws Exception {
                throw new Exception("deliberate");
            }
        };
        try {
            service.startAsync().awaitRunning();
            fail();
        } catch (RuntimeException e) {
            //assertSame(new Exception("deliberate"), e.getCause());
        }
        assertEquals(Service.State.FAILED, service.state());
    }

    @Test
    public void testStop_failed() throws Exception {
        final Exception exception = new Exception("deliberate");
        AbstractIdleService service = new DefaultService() {
            @Override
            protected void shutDown() throws Exception {
                throw exception;
            }
        };
        service.startAsync().awaitRunning();
        try {
            service.stopAsync().awaitTerminated();
            fail();
        } catch (RuntimeException e) {
            assertSame(exception, e.getCause());
        }
        assertEquals(Service.State.FAILED, service.state());
    }

}