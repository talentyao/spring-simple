package com.example.liuyaohua.guava.base;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.Map;

/**
 * 作者 yaohua.liu
 * 日期 2014-10-20 11:26
 *
 * 说明 ...
 */
public class JoinerTest {

    @Test
    public void testList() {
        // 连接对象数组
        Joiner joiner = Joiner.on(";").skipNulls();
        String str = joiner.join("Harry", null, "Ron", "Hermione");
        System.out.println(str);
    }

    @Test
    public void testMapJoiner() {
        // 连接Map
        Map<String, String> map = Maps.newHashMap();
        map.put("a", "1");
        map.put("b", "2");
        map.put("c", null);

        Joiner.MapJoiner joinerMap = Joiner.on(";").useForNull("XX").withKeyValueSeparator("=>");//键值之间的连接符
        String result = joinerMap.join(map);
        System.out.println(result);
    }
}
