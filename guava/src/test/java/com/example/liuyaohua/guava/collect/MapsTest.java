package com.example.liuyaohua.guava.collect;

import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.Map;

/**
 * 作者 yaohua.liu
 * 日期 2014-09-18 17:01
 * * 说明 ...
 */
public class MapsTest {
    @Test
    public void testMaps(){
        //newLinkedHashMap
        Map<Integer, Object> map = Maps.newHashMap();
        map.put(1, "a");
        map.put(2, "b");
        map.put(4, "d");

        // 过滤，保留没有过滤的Entry
        // filterKey、filterEntries、filterValues
        Map<Integer,Object> result= Maps.filterKeys(map, new Predicate<Object>() {
            @Override
            public boolean apply(Object input) {
                return !Objects.equal(input, 2);
            }
        });
        String str=result.toString();


        // 根据map的key与value转换成新的map类型对象 ，新的map的key与原map相同
        // EntryTransformer<Integer, Object, String> 第三个参数对应新map的value类型，第1、2个参数对应原map的key、value类型
        Map <Integer,String>tr = Maps.transformEntries(map, new Maps.EntryTransformer<Integer, Object, String>() {
            @Override
            public String transformEntry(Integer key, Object value) {
                return String.valueOf(key)+value;
            }
        });

        String str1=tr.toString();
    }
}
