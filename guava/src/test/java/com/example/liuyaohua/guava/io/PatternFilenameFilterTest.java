package com.example.liuyaohua.guava.io;

import com.google.common.io.PatternFilenameFilter;
import org.junit.Test;

import java.io.File;
import java.util.regex.Pattern;

/**
 * 作者 yaohua.liu
 * 日期 2014-09-26 18:09
 * * 说明 ...
 */
public class PatternFilenameFilterTest {

    @Test
    public void test(){
        Pattern pattern=Pattern.compile("s*txt");
        PatternFilenameFilter pf=new PatternFilenameFilter(pattern);
        File dir=new File(this.getClass().getClassLoader().getResource("").getPath());
        if(!dir.exists()){
            throw new IllegalStateException("文件不存在："+dir.getAbsolutePath());
        }
        if(!dir.isDirectory()){
            throw new IllegalArgumentException("路径不是目录"+dir.getPath());
        }
        System.out.println(pf.accept(dir, "source"));
    }
}
