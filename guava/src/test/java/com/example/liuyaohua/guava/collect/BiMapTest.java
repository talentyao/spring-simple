package com.example.liuyaohua.guava.collect;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.junit.Assert;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-21 17:43
 * * 说明 双向map 键与值都是唯一的，可以把键与值调换
 */
public class BiMapTest {
    @Test
    public void testBiMap() {

        // 反转key、value
        BiMap<Integer, String> biMap = HashBiMap.create();
        biMap.put(3, "Helium");
        Integer key = biMap.inverse().get("Helium");
        Assert.assertNotNull(key);
        Assert.assertEquals(3, key.intValue());
    }
}
