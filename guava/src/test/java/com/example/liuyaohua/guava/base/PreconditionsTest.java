package com.example.liuyaohua.guava.base;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2014-09-18 17:29
 * * 说明 ...
 */
public class PreconditionsTest {
    /**
     * 支持参数替换
     */
    @Test(expected = java.lang.IllegalStateException.class)
    public void testPreconditions() {

        // IllegalStateException
        Preconditions.checkState(Objects.equal("XX", "??"), "%s != %s", "XX", "??");

        // IllegalArgumentException
        Preconditions.checkArgument(1 == 2, "%s != %s", 1, 2);

        Preconditions.checkNotNull(null);

        /**
         * IndexOutOfBoundsException
         *
         * 可用于检测索引是否数组的长度内
         *
         * 第1个参数是index
         * 第2个参数是size
         */
        Preconditions.checkPositionIndex(1, 2);
    }
}
