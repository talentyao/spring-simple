package com.example.liuyaohua.guava.io;

import com.google.common.io.ByteStreams;
import com.google.common.io.Closeables;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-24 15:22
 * * 说明 A Closeable that collects Closeable resources and closes them all when it is closed
 */
public class CloseablesTest {
    @Test
    public void testCloseables() throws IOException {
        File file = new File(this.getClass().getClassLoader().getResource("").getPath() + "from.txt");

        // Avoid an extra allocation and copy.
        byte[] b = new byte[(int) file.length()];
        boolean threw = true;
        InputStream in = new FileInputStream(file);
        try {
            ByteStreams.readFully(in, b);
            threw = false;
        } finally {
            // 关闭流时如果有异常发生，当threw==false时抛异常，threw==ture时不抛异常
            Closeables.close(in, threw);//关闭in时，threw boolean Yes/false（抛出异常）
        }
    }
}
