package com.example.liuyaohua.guava.collect;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.sun.istack.internal.Nullable;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

/**
 * User yaohua.liu
 * Date 2014-09-12 14:03
 * Comments 用法类似Iterable
 */
public class IteratorsTest {
    @Test
    public void testIterators() {
        List<String> list = Lists.newArrayList("a","b","c");
        list.add(0, "0");

        Iterator<String> source = list.iterator();
        //过滤数据
        Iterator<String> result = Iterators.filter(source, new Predicate<String>() {
            @Override
            public boolean apply(@Nullable String s) {
                return !s.equals("b");
            }
        });
        // 使用for，需要新建List、Map然后再赋值，返回一个新的List或Map对象
        // 这里只要返回 resultList 即可

        List<String> resultList = Lists.newArrayList(result);
        String str=resultList.toString();
        System.out.println(str);
    }
}
