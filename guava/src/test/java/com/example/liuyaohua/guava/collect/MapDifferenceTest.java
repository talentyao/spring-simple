package com.example.liuyaohua.guava.collect;

import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.Map;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-21 17:57
 * * 说明 求map的并，交，差集
 */
public class MapDifferenceTest {
    /**
     * 求map的并，交，差集
     */
    @Test
    public void testMapDifference(){
        Map<Object, Object> mapA = Maps.newHashMap();
        Map<Integer,Object> mapB = Maps.newHashMap();

        mapA.put(1, "a");mapA.put(2, "b");mapA.put(4, "d");
        mapB.put(1, "a");mapB.put(3, "c");mapB.put(4, "c");
        MapDifference differenceMap = Maps.difference(mapA, mapB);

        // Only On Left
        Map entriesOnlyOnLeft = differenceMap.entriesOnlyOnLeft();
        String str2=entriesOnlyOnLeft.toString();

        // Only On Right
        Map entriesOnlyOnRight = differenceMap.entriesOnlyOnRight();
        String str3=entriesOnlyOnRight.toString();

        // Returns an unmodifiable map describing keys that appear in both maps,
        // but with different values.
        Map entriesDiffering = differenceMap.entriesDiffering();
        String str1=entriesDiffering.toString();

        // Returns an unmodifiable map containing the entries that appear in both maps;
        // that is, the intersection of the two maps.
        Map entriesInCommon = differenceMap.entriesInCommon();
        String str4=entriesInCommon.toString();
    }
}
