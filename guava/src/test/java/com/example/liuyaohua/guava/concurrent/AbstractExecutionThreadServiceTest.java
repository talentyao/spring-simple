package com.example.liuyaohua.guava.concurrent;

import com.google.common.base.Throwables;
import com.google.common.util.concurrent.AbstractExecutionThreadService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.Service;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-26 15:25
 * * 说明 单线程执行服务
 */
public class AbstractExecutionThreadServiceTest {

    private static class DefaultThreadService extends AbstractExecutionThreadService {
        @Override
        protected void run() throws Exception {
            //latch.await();
            System.out.println("DefaultThreadService run========================================");
        }

    }

    @Test
    public void testAbstractExecutionThreadService() throws Exception {
        DefaultThreadService service =new DefaultThreadService();
        service.addListener(new Service.Listener() {
            @Override
            public void starting() {
                //super.starting();
                System.out.println("Listener（starting）");
            }
            @Override
            public void running() {
                //super.running();
                System.out.println("Listener（running）：");
            }

            @Override
            public void stopping(Service.State from) {
                //super.stopping(from);
                System.out.println("Listener（stopping）:"+from.toString());
            }

            @Override
            public void terminated(Service.State from) {
                //super.terminated(from);
                System.out.println("Listener（terminated）:"+from.toString());
            }

            @Override
            public void failed(Service.State from, Throwable failure) {
                //super.failed(from,failure);
                System.out.println("Listener（failed）:" + from.toString());
                Throwables.propagate(failure);
            }
        }, MoreExecutors.sameThreadExecutor());
        //service.addListener();

        System.out.println("==============new===================");
        System.out.println("test："+service.state());
        assertEquals(Service.State.NEW, service.state());

        System.out.println("==============startAsync===================");
        service.startAsync();
        System.out.println("test："+service.state());
        //assertEquals(Service.State.RUNNING,service.state());

        System.out.println("==============awaitRunning===================");
        service.awaitRunning();
        System.out.println("test："+service.state());
        //assertEquals(Service.State.RUNNING,service.state());

        System.out.println("==============stopAsync===================");
        service.stopAsync();
        System.out.println("test："+service.state());
//        assertEquals(Service.State.STOPPING,service.state());

        System.out.println("==============awaitTerminated===================");
        service.awaitTerminated();
        System.out.println("test："+service.state());
        assertEquals(Service.State.TERMINATED, service.state());
    }
}
